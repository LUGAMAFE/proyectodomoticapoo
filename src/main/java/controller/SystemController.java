package controller;

import model.dbModels.LugaresModel;
import model.dbModels.UsersModel;
import model.persistencia.GestorPersistencia;
import model.schemas.*;

import java.util.List;

public class SystemController {
    private GestorPersistencia GestorBD;

    public SystemController() {
        this.GestorBD = new GestorPersistencia("ProyectoDomotica");
    }

    public void initExampleSetUPCasa(){
        LugaresModel lugaresModel = new LugaresModel(this.GestorBD, Lugar.class);
        UsersModel usuariosModel = new UsersModel(this.GestorBD, User.class);

        //Definicion de la casa
        Lugar casa = this.definicionCasa(lugaresModel);
        //Creacion del usuario
        Administrador admin = this.crearUnUsuario(usuariosModel);
        //Añadir Lugar a administrador
        admin.addLugar(casa);
        //Se obtienen el unico lugar asociado al administrador
        List<Lugar> lugares = admin.getLugares();
        Lugar casaLuis = lugares.get(0);

        //Se encienden todos los aparatos de la casa ya que por defecto todos estan apagados.
        casaLuis.toggleEncendido(true);

        //Se busca la planta alta y se apaga toda la planta
        Zona sala = casaLuis.searchZone("Planta Alta");
        if(sala != null){
            sala.toggleEncendido(false);
        }

        //Se busca la recamara principal y se prende toda la recamara
        Habitacion room = casaLuis.searchHabitacion("Recamara principal");
        if(room != null){
            room.toggleEncendido(true);
            //Se apa la tele de la recamara
            Dispositivo tv2 = room.searchDispositivo("TV2");
            if(tv2 != null){
                tv2.setEncendido(false);
            }
        }

        lugaresModel.update(casaLuis);
    }

    public Administrador crearUnUsuario(UsersModel usuariosModel){
        Persona luis = new Persona("Luis Javier", "Martinez", "Fernandez");
        Administrador admin = new Administrador(luis, "LUGAMAFE", "luisjavier004@hotmail.com", "123456", 1, "si");
        usuariosModel.insert(admin);
        return admin;
    }

    public Lugar definicionCasa(LugaresModel lugaresModel) {
        Lugar casa = new Lugar("Casa" , true, "Mi casa es muy bonita");
        lugaresModel.insert(casa);

        Zona plantaAlta = new Zona("Planta Alta", true, "Zona de arriba de la casa");
        Zona plantaBaja = new Zona("Planta Baja", true, "Zona de abajo de la casa");

        casa.addZona(plantaAlta);
        casa.addZona(plantaBaja);

        Habitacion sala = new Habitacion("Sala", true, "Sala Principal de la casa");
        Habitacion cocina = new Habitacion("Cocina", true, "Cocina de la casa el lugar mas delicioso");
        Habitacion comedor = new Habitacion("Comedor", true, "Aqui es donde toda la familia come y esta la mesa mas grande de la casa");
        Habitacion recamara = new Habitacion("Recamara principal", true, "Recamara principal de la casa");
        Habitacion bano = new Habitacion("Baño", true, "Baño de uso exclusivo para la recamara principal");

        casa.addHabitacion(sala);
        casa.addHabitacion(cocina);
        casa.addHabitacion(comedor);
        casa.addHabitacion(recamara);
        casa.addHabitacion(bano);

        plantaAlta.addHabitacion(recamara);
        plantaAlta.addHabitacion(bano);
        plantaBaja.addHabitacion(sala);
        plantaBaja.addHabitacion(cocina);
        plantaBaja.addHabitacion(comedor);

        Dispositivo tv = new Dispositivo("TV 1", true, "Smart TV Samsung");
        Dispositivo foco1 = new Dispositivo("Foco 1", true, "Foco Amarillo");
        Dispositivo AC = new Dispositivo("Aire Acondicionado", true, "Aire Acondicionado Samsung destinado a la cocina");
        Dispositivo foco2 = new Dispositivo("Foco 2", true, "Foco Amarillo");
        Dispositivo tv2 = new Dispositivo("TV 2", true, "Apple TV");
        Dispositivo foco3 = new Dispositivo("Foco 3", true, "Foco Amarillo");
        Dispositivo foco4 = new Dispositivo("Foco 4", true, "Foco Amarillo");

        sala.addDispositivo(tv);
        sala.addDispositivo(foco1);
        cocina.addDispositivo(AC);
        cocina.addDispositivo(foco2);
        comedor.addDispositivo(foco3);
        recamara.addDispositivo(tv2);
        recamara.addDispositivo(foco3);
        bano.addDispositivo(foco4);

        lugaresModel.update(casa);

        return casa;
    }
}
