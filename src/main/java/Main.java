import controller.SystemController;

/**
 *
 * @author Luis Martinez
 */
public class Main {

    public static void main(String[] args) {
        SystemController systemController = new SystemController();
        systemController.initExampleSetUPCasa();
    }
}
