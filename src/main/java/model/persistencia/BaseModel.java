package model.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class BaseModel<T> {
    private GestorPersistencia gestorBD;
    private Class<T> entitySchemaClass;
    private EntityManager em;
    private EntityTransaction actualTransaction;

    public BaseModel(GestorPersistencia gestorBD, Class<T> entitySchemaClass) {
        this.gestorBD = gestorBD;
        this.entitySchemaClass = entitySchemaClass;
        //Se crea Un Entity Manager para la clase
        this.em =  this.gestorBD.createEntityManager();
    }

    public void checkEntityManager(){
        if(!emIsOpen()){
            this.em =  this.gestorBD.createEntityManager();
        }
    }

    public boolean emIsOpen(){
        return this.em.isOpen();
    }

    public void closeEm(){
        this.em.close();
    }


    public T getByID(T primaryKey){
        return this.em.find(this.entitySchemaClass, primaryKey);
    }

    public boolean insert(Object objectTo) {
        this.checkEntityManager();
        objectTo = this.entitySchemaClass.cast(objectTo);
        this.actualTransaction = this.em.getTransaction();
        this.actualTransaction.begin();
        this.em.persist(objectTo);
        try {
            this.actualTransaction.commit();
        } catch (Exception ex) {
            return false;
        } finally {
            this.closeEm();
        }
        return true;
    }

    public boolean update(Object objectTo) {
        this.checkEntityManager();
        objectTo = this.entitySchemaClass.cast(objectTo);
        this.actualTransaction = this.em.getTransaction();
        this.actualTransaction.begin();
        try {
            this.em.persist(this.em.merge(objectTo));
            this.actualTransaction.commit();
        } catch (Exception ex) {
            return false;
        } finally {
            this.em.close();
        }
        return true;
    }

    public boolean delete(Object objectTo) {
        this.checkEntityManager();
        objectTo = this.entitySchemaClass.cast(objectTo);
        this.actualTransaction = this.em.getTransaction();
        this.actualTransaction.begin();
        try {
            this.em.remove(this.em.merge(objectTo));
            this.actualTransaction.commit();
        } catch (Exception ex) {
            return false;
        } finally {
            this.em.close();
        }
        return true;
    }
}
