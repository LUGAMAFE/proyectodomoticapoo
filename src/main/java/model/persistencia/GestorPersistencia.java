package model.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GestorPersistencia {
    EntityManagerFactory fabrica;

    public GestorPersistencia(String databaseName) {
        this.fabrica = Persistence.createEntityManagerFactory(databaseName);
    }

    public EntityManager createEntityManager(){
        return this.fabrica.createEntityManager();
    }

    public void cerrar() {
        this.fabrica.close();
    }
}
