package model.schemas;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "lugares")
public class Lugar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_lugar")
    private int id;
    @Column(name = "nombre_lugar")
    private String nombre;
    @Column(name = "habilitado_lugar")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean habilitado;
    @Column(name = "descripcion_lugar")
    private String descripcion;

    @OneToMany(
            mappedBy = "lugar",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Zona> zonas = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Administrador administrador;

    @OneToMany(
            mappedBy = "lugar",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Habitacion> habitaciones = new ArrayList<>();

    public Lugar() {
    }

    public Lugar(String nombre, boolean habilitado, String descripcion) {
        this.nombre = nombre;
        this.habilitado = habilitado;
        this.descripcion = descripcion;
    }

    public void addZona(Zona zona) {
        zonas.add(zona);
        zona.setLugar(this);
    }

    public void removeZona(Zona zona) {
        zonas.remove(zona);
        zona.setLugar(null);
    }

    public void addHabitacion(Habitacion habitacion) {
        habitaciones.add(habitacion);
        habitacion.setLugar(this);
    }

    public void removeHabitacion(Habitacion habitacion) {
        habitaciones.remove(habitacion);
        habitacion.setLugar(null);
    }

    public List<Habitacion> getHabitaciones() {
        return habitaciones;
    }

    public void toggleEncendido(boolean encender){
        for(int i = 0; i < this.habitaciones.size(); i++){
            Habitacion habitacion = this.habitaciones.get(i);
            habitacion.toggleEncendido(encender);
        }
    }

    public Dispositivo searchDispositivo(String deviceName){
        for(int i = 0; i < this.habitaciones.size(); i++){
            Habitacion habitacion = this.habitaciones.get(i);
            Dispositivo dispositivo = habitacion.searchDispositivo(deviceName);
            if(dispositivo != null){
                return dispositivo;
            }
        }
        return null;
    }

    public Zona searchZone(String zoneName){
        for(int i = 0; i < this.zonas.size(); i++){
            if(this.zonas.get(i).getNombre().equals(zoneName)){
                return this.zonas.get(i);
            }
        }
        return null;
    }

    public Habitacion searchHabitacion(String roomName){
        for(int i = 0; i < this.habitaciones.size(); i++){
            if(this.habitaciones.get(i).getNombre().equals(roomName)){
                return this.habitaciones.get(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Lugar{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", habilitado=" + habilitado +
                ", descripcion='" + descripcion + '\'' +
                ", zonas=" + zonas +
                '}';
    }
}
