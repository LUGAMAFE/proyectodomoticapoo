package model.schemas;
import javax.persistence.*;

@MappedSuperclass
public class User {
    @Id
    @Column(name = "id_user")
    private int id;
    @Column(name = "username_user")
    private String username;
    @Column(name = "email_user")
    private String email;
    @Column(name = "password_user")
    private String password;
    @Column(name = "user_question_user")
    private int question;
    @Column(name = "answer_user")
    private String answer;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Persona persona;

    public User(){}

    public User(Persona persona, String username, String email, String password, int question, String answer) {
        this.persona = persona;
        this.username = username;
        this.email = email;
        this.password = password;
        this.question = question;
        this.answer = answer;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return username;
    }
    public void setName(String name) {
        this.username = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public int getQuestion() {
        return question;
    }
    public void setQuestion(int question) {
        this.question = question;
    }
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", question=" + question +
                ", answer='" + answer + '\'' +
                '}';
    }
}