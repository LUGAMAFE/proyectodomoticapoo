package model.schemas;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "habitaciones")
public class Habitacion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_habitacion")
    private int id;
    @Column(name = "nombre_habitacion")
    private String nombre;
    @Column(name = "habilitado_habitacion")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean habilitada;
    @Column(name = "descripcion_habitacion")
    private String descripcion;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "habitacion_id_dispositivo")
    private List<Dispositivo> dispositivos = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Lugar lugar;


    public Habitacion() {
    }

    public Habitacion(String nombre, boolean habilitada, String descripcion) {
        this.nombre = nombre;
        this.habilitada = habilitada;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isHabilitada() {
        return habilitada;
    }

    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public void addDispositivo(Dispositivo dispositivo) {
        dispositivos.add(dispositivo);
    }

    public void removeDispositivo(Dispositivo dispositivo) {
        dispositivos.remove(dispositivo);
    }

    public List<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public void toggleEncendido(boolean encender){
        List<Dispositivo> dispositivosHabitacion = this.dispositivos;
        for (int j = 0; j < dispositivosHabitacion.size(); j++){
            dispositivosHabitacion.get(j).setEncendido(encender);
        }
    }

    public Dispositivo searchDispositivo(String deviceName) {
        for(int i = 0; i < this.dispositivos.size(); i++){
            if(this.dispositivos.get(i).getNombre().equals(deviceName)){
                return this.dispositivos.get(i);
            }
        }
        return null;
    }

    public void setDispositivos(List<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
    }

    @Override
    public String toString() {
        return "Habitacion{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", habilitada=" + habilitada +
                ", descripcion='" + descripcion + '\'' +
                ", lugar=" + lugar +
                '}';
    }
}
