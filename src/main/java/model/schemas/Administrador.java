package model.schemas;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class Administrador extends User {

    @OneToMany(
            mappedBy = "administrador",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Lugar> lugares = new ArrayList<>();

    public Administrador() {
    }

    public Administrador(Persona persona, String username, String email, String password, int question, String answer) {
        super(persona, username, email, password, question, answer);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void addLugar(Lugar lugar){
        lugares.add(lugar);
    }

    public List<Lugar> getLugares() {
        return lugares;
    }
}
