package model.schemas;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "dispositivos")
public class Dispositivo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dispositivo")
    private int id;
    @Column(name = "nombre_dispositivo")
    private String nombre;
    @Column(name = "habilitado_dispositivo")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean habilitada;
    @Column(name = "descripcion_dispositivo")
    private String descripcion;
    @Column(name = "encendido_dispositivo")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean encendido = false;

    public Dispositivo() {
    }

    public Dispositivo(String nombre, boolean habilitada, String descripcion) {
        this.nombre = nombre;
        this.habilitada = habilitada;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isHabilitada() {
        return habilitada;
    }

    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

}
