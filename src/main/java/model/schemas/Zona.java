package model.schemas;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "zonas")
public class Zona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zona")
    private int id;
    @Column(name = "nombre_zona")
    private String nombre;
    @Column(name = "habilitado_zona")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean habilitada;
    @Column(name = "descripcion_zona")
    private String descripcion;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lugar lugar;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parent_zona")
    private Zona parent;

    @OneToMany(mappedBy="parent", cascade={CascadeType.ALL})
    private List<Zona> zonas = new ArrayList<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "id_zona_lugar")
    private List<Habitacion> habitaciones = new ArrayList<>();

    public Zona() {
    }

    public Zona(String nombre, boolean habilitada, String descripcion){
        this.nombre = nombre;
        this.habilitada = habilitada;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isHabilitada() {
        return habilitada;
    }

    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public Zona getParent() {
        return parent;
    }

    public void setParent(Zona parent) {
        this.parent = parent;
    }

    public List<Zona> getZonas() {
        return zonas;
    }

    public void addZona(Zona zona) {
        zona.setLugar(this.getLugar());
        zonas.add(zona);
    }

    public void removeZona(Zona zona) {
        zona.setLugar(null);
        zonas.remove(zona);
    }

    public void addHabitacion(Habitacion habitacion) {
        habitaciones.add(habitacion);
    }

    public void removeHabitacion(Habitacion habitacion) {
        habitaciones.remove(habitacion);
    }

    public void toggleEncendido(boolean encender){
        for(int i = 0; i < this.habitaciones.size(); i++){
            Habitacion habitacion = this.habitaciones.get(i);
            habitacion.toggleEncendido(encender);
        }
    }

}
